import http from 'http'

import logger from './utils/logger'
import app from './app'

const port = process.env.PORT || '3000'

const server = http.createServer(app.callback()).listen(port, (err: Error) => {
  if (err) {
    logger.error('Error occured on starting the server', err)
    return
  }
  logger.info(`Server running successfully on port: ${port}`)
})

export default server
