import Koa from 'koa'
import bodyParser from 'koa-bodyparser-ts'
import helmet from 'koa-helmet'
import cors from '@koa/cors'
import logger from 'koa-logger'
import responseTime from 'koa-response-time'

import setupV1Routes from './routes-v1'
import setupErrorHandler from './error-handler'

const app = new Koa()

setupErrorHandler(app)

app.use(responseTime())
process.env.NODE_ENV !== 'test' && app.use(logger())

app.use(helmet())
app.use(cors())

app.use(bodyParser())

setupV1Routes(app)

export default app
