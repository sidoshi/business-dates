import * as winston from 'winston'

const logger = winston.createLogger({
  transports: [new winston.transports.Console()],
  format: winston.format.simple(),
  level: process.env.LOG_LEVEL || 'info',
})

export default logger
