import { DateTime } from 'luxon'
import Holidays, { Holiday } from 'date-holidays'

interface HolidaysCache {
  [year: string]: Holiday[]
}

const holidaysCache: HolidaysCache = {}

export const holidaysBetween = (from: DateTime, to: DateTime): number => {
  const hd = new Holidays('US', undefined, undefined, { timezone: 'UTC' })

  const fromYear = from.get('year')
  const toYear = to.get('year')

  if (!holidaysCache[fromYear]) {
    holidaysCache[fromYear] = hd
      .getHolidays(toYear.toString())
      .filter(holiday => holiday.type === 'public')
  }
  if (!holidaysCache[toYear]) {
    holidaysCache[toYear] = hd
      .getHolidays(toYear.toString())
      .filter(holiday => holiday.type === 'public')
  }

  // TODO:
  // Here we make an assumption that there will be maximum 1 year gap
  // between the two given dates. We have to either consider all the years
  // in between, or only support a limited gap and throw when the gap
  // exceeds limit.
  const holidays = [
    ...holidaysCache[fromYear],
    ...(fromYear === toYear ? [] : holidaysCache[toYear]),
  ]

  return holidays.reduce((n, holiday) => {
    const date = DateTime.fromJSDate(holiday.start)

    if (date >= from && date < to) {
      return n + 1
    }
    return n
  }, 0)
}

export const isHoliday = (date: DateTime): boolean => {
  const hd = new Holidays('US', undefined, undefined, { timezone: 'UTC' })

  const holiday = hd.isHoliday(date.toJSDate())
  if (holiday && holiday.type === 'public') {
    return true
  }
  return false
}
