import Koa from 'koa'
import Router from 'koa-router'
import mount from 'koa-mount'

const routers: Router[] = [
  require('./businessDates').default, //
]

export default (app: Koa) => {
  routers.forEach(router => {
    app
      .use(mount('/api/v1', router.routes()))
      .use(mount('/api/v1', router.allowedMethods()))
  })
}
