import Router from 'koa-router'
import Joi from 'joi'

import validate from '../middlewares/validate'
import { getBusinessDateWithDelayP, isBusinessDay } from '../core/businessDates'

const router = new Router({
  prefix: '/businessDates',
})

const getBusinessDateWithDelaySchema = {
  initialDate: Joi.date()
    .iso()
    .required(),
  delay: Joi.number().required(),
}

router
  .get(
    '/getBusinessDateWithDelay',
    validate({
      query: getBusinessDateWithDelaySchema,
    }),
    async ctx => {
      ctx.body = {
        ok: true,
        initialQuery: ctx.query,
        results: getBusinessDateWithDelayP(ctx.query),
      }
    }
  )
  .post(
    '/getBusinessDateWithDelay',
    validate({
      body: getBusinessDateWithDelaySchema,
    }),
    async ctx => {
      ctx.body = {
        ok: true,
        initialQuery: ctx.request.body,
        results: getBusinessDateWithDelayP(ctx.request.body),
      }
    }
  )

const isBusinessDaySchema = {
  date: Joi.date()
    .iso()
    .required(),
}

router
  .get(
    '/isBusinessDay',
    validate({
      query: isBusinessDaySchema,
    }),
    async ctx => {
      ctx.body = {
        ok: true,
        initialQuery: ctx.query,
        results: isBusinessDay(ctx.query),
      }
    }
  )
  .post(
    '/isBusinessDay',
    validate({
      body: isBusinessDaySchema,
    }),
    async ctx => {
      ctx.body = {
        ok: true,
        initialQuery: ctx.request.body,
        results: isBusinessDay(ctx.query),
      }
    }
  )

export default router
