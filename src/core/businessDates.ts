import { DateTime } from 'luxon'
import bizniz from 'bizniz'
import postal from 'postal'

import { holidaysBetween, isHoliday } from '../utils/holidays'

export interface GetBusinessDateWithDelayOptions {
  initialDate: string
  delay: string
}

export interface GetBusinessDateWithDelayResult {
  businessDate: string
  totalDays: number
  holidayDays: number
  weekendDays: number
}

export const getBusinessDateWithDelay = (
  options: GetBusinessDateWithDelayOptions
): GetBusinessDateWithDelayResult => {
  const date = DateTime.fromISO(options.initialDate).toUTC()
  const delay = Number.parseInt(options.delay, 10)

  const delayedDate = date.plus({
    days: delay,
  })

  const weekendsInBetween = bizniz.weekendDaysBetween(
    date.toJSDate(),
    delayedDate.toJSDate()
  )
  const holidaysInBetween = holidaysBetween(date, delayedDate)

  const totalDelay = delay + weekendsInBetween + holidaysInBetween

  // Total delay is only greater than delay when there are holidays and
  // weekends in between. We can't just shift the date by total delay since
  // there might be non-business days in between as well.
  // For Eg:
  // For initialDate = `2018-11-15` and delay = `3`,
  // The initial delayed date without accounting for business date would be:
  // `2018-11-17`. This is because we want it to be initialDate inclusive.
  //
  // There is one weekend between `2018-11-15` and `2018-11-17` as `2018-11-17`
  // is `Saturday`. Now if we only add one to the delay, the final business date
  // would be `2018-11-18` which is Sunday and not a business day. So we need to
  // keep shift the dates compoundly until there are no holidays in between.
  if (totalDelay > delay) {
    const compound = getBusinessDateWithDelay({
      initialDate: delayedDate.toISO(),
      delay: (totalDelay - delay).toString(),
    })

    return {
      businessDate: compound.businessDate,
      totalDays: totalDelay + compound.holidayDays + compound.weekendDays,
      holidayDays: holidaysInBetween + compound.holidayDays,
      weekendDays: weekendsInBetween + compound.weekendDays,
    }
  }

  // We add totalDelay - 1 days because luxon excludes the first date and we
  // want to include it.
  // So without subtracting 1, Aug 15 + 3 days would be Aug 18: 16, 17, 18
  // We want it to be Aug 17: 15, 16, 17
  const businessDateWithDelay = date.plus({
    days: totalDelay - 1,
  })
  return {
    businessDate: businessDateWithDelay.toISO(),
    totalDays: totalDelay,
    holidayDays: holidaysInBetween,
    weekendDays: weekendsInBetween,
  }
}

export interface IsBusinessDayOptions {
  date: string
}

export interface IsBusinessDayResult {
  isBusinessDay: boolean
  isHoliday: boolean
  isWeekend: boolean
}

export const isBusinessDay = (
  options: IsBusinessDayOptions
): IsBusinessDayResult => {
  const date = DateTime.fromISO(options.date).toUTC()
  const result: IsBusinessDayResult = {
    isBusinessDay: true,
    isHoliday: false,
    isWeekend: false,
  }

  if (bizniz.isWeekendDay(date.toJSDate())) {
    result.isWeekend = true
  }

  if (isHoliday(date)) {
    result.isHoliday = true
  }

  if (result.isWeekend || result.isHoliday) {
    result.isBusinessDay = false
  }

  return result
}

// This works as a proxy to the `getBusinessWithDelay` f
// function. This should be used when we want to publish
// the calculation to the channel as well.
export const getBusinessDateWithDelayP = (
  options: GetBusinessDateWithDelayOptions
): GetBusinessDateWithDelayResult => {
  const businessDateWithDelay = getBusinessDateWithDelay(options)

  postal.publish({
    channel: 'BankWire',
    topic: 'businessDates',
    data: {
      ok: true,
      initialQuery: options,
      data: businessDateWithDelay,
    },
  })

  return businessDateWithDelay
}
