import { Middleware, Context } from 'koa'
import { validate, SchemaLike } from 'joi'

export interface ValidatorOptions {
  body?: SchemaLike
  params?: SchemaLike
  headers?: SchemaLike
  query?: SchemaLike
}

export default (schema: ValidatorOptions): Middleware => async (
  ctx: Context,
  next
) => {
  try {
    const obj: ValidatorOptions = {}

    if (schema.body) obj.body = ctx.request.body || {}
    if (schema.params) obj.params = ctx.params || {}
    if (schema.headers) obj.headers = ctx.headers || {}
    if (schema.query) obj.query = ctx.query || {}

    await validate(obj, schema)

    next()
  } catch (error) {
    const detail = error.details && error.details[0]
    const message = detail ? detail.message : 'Bad Request'
    ctx.throw(400, message)
  }
}
