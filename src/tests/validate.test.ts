import * as sinon from 'sinon'
import { Context } from 'koa'
import Joi from 'joi'
import { expect } from 'chai'

import validate from '../middlewares/validate'

const createCtx = (obj: any): Context => ({
  throw: (status: number, message: string) => {
    const err: any = new Error(message)
    err.status = status
    throw err
  },
  request: {},
  ...obj,
})

it('should validate request properly', async () => {
  const middleware = validate({
    body: Joi.object().keys({
      test: Joi.string().required(),
    }),
  })
  const next = sinon.spy()

  try {
    await middleware(createCtx({}), next)
  } catch (err) {
    expect(err.message).to.eql('"test" is required')
  }

  try {
    await middleware(
      createCtx({
        request: {
          body: {
            test: 'pass',
          },
        },
      }),
      next
    )
  } catch (err) {
    expect.fail()
  }
})
