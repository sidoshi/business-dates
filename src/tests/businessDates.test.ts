import chai from 'chai'
import request from 'supertest'

import server from '../index'

const base = '/api/v1/businessDates'
const expect = chai.expect

describe('businessDates - API', () => {
  describe('getBusinessDateWithDelay', () => {
    const url = `${base}/getBusinessDateWithDelay`

    it('should not be 404', async () => {
      let res = await request(server).get(url)
      expect(res.status).not.equal(404)
      expect(res.status).not.equal(405)
      res = await request(server).post(url)
      expect(res.status).not.equal(404)
      expect(res.status).not.equal(405)
    })

    it('should validate input properly', async () => {
      let res = await request(server).get(url)
      expect(res.status).to.equal(400)

      const validDate = '2018-11-10T00:00:00.000Z'
      const invalidDate = '20-11-2010T00:00:00.000Z'

      const validBody = { initialDate: validDate, delay: '3' }
      const invalidBody = { initialDate: invalidDate, delay: '3' }

      res = await request(server)
        .get(url)
        .query(invalidBody)
      expect(res.status).to.equal(400)

      res = await request(server)
        .get(url)
        .query(validBody)
      expect(res.status).to.equal(200)

      res = await request(server)
        .post(url)
        .send(validBody)
      expect(res.status).to.equal(200)
    })

    it('should calculate business dates properly', async () => {
      let body = { initialDate: '2018-11-10T00:00:00.000Z', delay: '3' }

      let res = await request(server)
        .get(url)
        .query(body)
      expect(res.status).to.equal(200)

      expect(res.body).to.eql({
        ok: true,
        initialQuery: body,
        results: {
          businessDate: '2018-11-15T00:00:00.000Z',
          totalDays: 6,
          holidayDays: 1,
          weekendDays: 2,
        },
      })

      body = { initialDate: '2018-11-15T00:00:00.000Z', delay: '3' }
      res = await request(server)
        .post(url)
        .send(body)
      expect(res.status).to.equal(200)
      expect(res.body).to.eql({
        ok: true,
        initialQuery: body,
        results: {
          businessDate: '2018-11-19T00:00:00.000Z',
          totalDays: 5,
          holidayDays: 0,
          weekendDays: 2,
        },
      })

      body = { initialDate: '2018-12-25T00:00:00.000Z', delay: '20' }
      res = await request(server)
        .post(url)
        .send(body)
      expect(res.status).to.equal(200)
      expect(res.body).to.eql({
        ok: true,
        initialQuery: body,
        results: {
          businessDate: '2019-01-24T00:00:00.000Z',
          totalDays: 31,
          holidayDays: 3,
          weekendDays: 8,
        },
      })
    })
  })

  describe('isBusinessDay', () => {
    const url = `${base}/isBusinessDay`

    it('should not be 404', async () => {
      let res = await request(server).get(url)
      expect(res.status).not.equal(404)
      expect(res.status).not.equal(405)
      res = await request(server).post(url)
      expect(res.status).not.equal(404)
      expect(res.status).not.equal(405)
    })

    it('should validate input properly', async () => {
      let res = await request(server).get(url)
      expect(res.status).to.equal(400)

      const validDate = '2018-11-10T00:00:00.000Z'
      const invalidDate = '20-11-2010T00:00:00.000Z'

      const validBody = { date: validDate }
      const invalidBody = { date: invalidDate }

      res = await request(server)
        .get(url)
        .query(invalidBody)
      expect(res.status).to.equal(400)

      res = await request(server)
        .get(url)
        .query(validBody)
      expect(res.status).to.equal(200)

      res = await request(server)
        .post(url)
        .send(validBody)
      expect(res.status).to.equal(200)
    })

    it('should check if the date is a business date', async () => {
      let body = { date: '2018-11-09T00:00:00.000Z' }
      let res = await request(server)
        .get(url)
        .query(body)
      expect(res.status).to.equal(200)

      expect(res.body).to.eql({
        ok: true,
        initialQuery: body,
        results: {
          isBusinessDay: true,
          isHoliday: false,
          isWeekend: false,
        },
      })

      body = { date: '2018-11-10T00:00:00.000Z' }
      res = await request(server)
        .get(url)
        .query(body)
      expect(res.status).to.equal(200)

      expect(res.body).to.eql({
        ok: true,
        initialQuery: body,
        results: {
          isBusinessDay: false,
          isHoliday: false,
          isWeekend: true,
        },
      })

      body = { date: '2017-11-11T00:00:00.000Z' }
      res = await request(server)
        .get(url)
        .query(body)
      expect(res.status).to.equal(200)

      expect(res.body).to.eql({
        ok: true,
        initialQuery: body,
        results: {
          isBusinessDay: false,
          isHoliday: true,
          isWeekend: true,
        },
      })
    })
  })
})
