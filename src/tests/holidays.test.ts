import { DateTime } from 'luxon'
import { expect } from 'chai'

import { holidaysBetween, isHoliday } from '../utils/holidays'

describe('holidays', () => {
  describe('holidaysBetween', () => {
    it('should return no of holidays between to days', () => {
      expect(
        holidaysBetween(DateTime.utc(2018), DateTime.utc(2018).endOf('year'))
      ).to.eql(10)

      expect(
        holidaysBetween(DateTime.utc(2018, 5, 28), DateTime.utc(2018, 9, 3))
      ).to.eql(2)

      expect(
        holidaysBetween(DateTime.utc(2018, 12, 25), DateTime.utc(2019, 11, 30))
      ).to.eql(10)
    })
  })

  describe('isHoliday', () => {
    it('should return no of holidays between to days', () => {
      expect(isHoliday(DateTime.utc(2018, 1, 10))).to.equal(false)
      expect(isHoliday(DateTime.utc(2018, 1, 1))).to.equal(true)
      expect(isHoliday(DateTime.utc(2017, 11, 11))).to.equal(true)
    })
  })
})
