import errorHandler from 'koa-json-error'
import Koa from 'koa'

import logger from './utils/logger'

export interface KoaError extends Error {
  expose: boolean
  status: number
}

export interface FormattedError {
  status: number
  message: string
}

export const format = (err: KoaError): FormattedError => {
  // expose flag is set to true when we `ctx.throw` is used with
  // error code < 500. This error messages are safe to be sent to clients.
  if (err.expose) {
    return {
      status: err.status,
      message: err.message,
    }
  }

  // Other error messages are not sent to client as they may contain
  // sensetive data.
  return {
    status: 500,
    message: 'Internal Server Error',
  }
}

export default (app: Koa) => {
  app.on('error', err => {
    logger.error('Internal Server Error', err)
  })
  app.use(errorHandler({ format }))
}
