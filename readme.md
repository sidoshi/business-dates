# business-dates

> Business Date Utilities Service

This microservice exposes utilites to calculate business dates.

## REST API

```
Base URL:- /api/v1/businessDates
```

<hr />

**`/getBusinessDateWithDelay`** - **GET, POST**

<br>
Get the business date after calculating the given
delay.

Example Request:

```
{
  "initialDate": "2018-11-10T00:00:00Z",
  "delay": 3
}
```

Example Response:

```
{
  "ok": true,
  "initialQuery": {
    "initialDate": "2018-11-15T00:00:00Z",
    "delay": 3
  },
  "results": {
    "businessDate": "2018-12-15T10:10:10Z",
    "totalDays": 6,
    "holidayDays": 1,
    "weekendDays": 2
  }
}
```

The endpoint also publishes on a Pub/Sub interface based on `postal.js`.

Channel:- `BankWire`
Topic:- `businessDates`

Data sent to the subscribed clients is in the same format as the rest api.

<hr />

**`/isBusinessDay`** - **GET, POST**

<br>
Check if the given date is a business date.

Example Request:

```
{
  "date": "2018-11-12T00:00:00Z"
}
```

Example Response"

```
{
  "ok": true,
  "initialQuery": {
    "initialDate": "2018-11-12T00:00:00Z",
  },
  "results": {
    "isBusinessDay": false,
    "isHoliday": true,
    "isWeekend": false,
  }
}
```

<hr />

## License

MIT © [Siddharth Doshi](https://sid.sh)
